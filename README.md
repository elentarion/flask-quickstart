# Tesseract: a quickstart Flask application

## Create your model

Start by creating your table structure in "schema.sql". This file will be loaded
with the command line later on through the command "flask initdb".