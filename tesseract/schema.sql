drop table if exists sources;
create table sources (
  id integer primary key,
  name text not null,
  url text not null,
  last_checked text
);