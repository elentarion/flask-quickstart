import os
import sqlite3
import pprint

from flask import Flask, request, session, g, abort, render_template
from werkzeug.utils import find_modules, import_string

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'tesseract.db'),
    SECRET_KEY='Z3ks4PxS2EmRERhWfaEc9JXyqBYxmG8WjjH98yjVbAqstZM6zczwqTyQjXtS5Tr'
))

def connect_db():
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv
    
def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db
    
@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()
        
def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
    init_db()
    print('Initialized the database.')

for name in find_modules('tesseract.blueprints'):
    mod = import_string(name)
    blueprint_name = name.split('.')[-1]
    if hasattr(mod, blueprint_name):
        url_prefix = url_prefix='/' + blueprint_name
        if blueprint_name[0] == '_':
            url_prefix="/"
            
        app.register_blueprint(
            getattr(mod, blueprint_name),
            url_prefix=url_prefix
        )
        print('Your module "{}" has been automatically registered as a'        \
        ' blueprint. You can access it under the "{}" url.'.format(name,
        url_prefix))
    else:
        print('Your module "{}" has no attribute named "{}". If you want your' \
        'blueprints to be automatically registered you should name them after' \
        'the module name you put them in.'.format(name, url_prefix))
    