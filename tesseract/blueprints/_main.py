from flask import Blueprint, render_template

_main = Blueprint('_main', __name__,)

@_main.route('/')
def index():
    return render_template('index.html')