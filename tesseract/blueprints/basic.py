from flask import Blueprint, render_template

basic = Blueprint('basic', __name__, template_folder='templates/basic')

@basic.route('/')
def index():
    return render_template('basic.html')
