from setuptools import setup

setup(
    name='tesseract',
    packages=['tesseract'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)